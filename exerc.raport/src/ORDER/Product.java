package ORDER;

import REPORT.Visitable;
import REPORT.Visitor;

public class Product implements Visitable {

	private  String name;
	private double price;
	
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public double getPrice() 
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
	@Override
	public void accept(Visitor visitor) 
	{
		Visitor.visit(this);
	}
	@Override
	public String giveReport() 
	{
		return "product: "+name + ", price: "+price;
	}
}
