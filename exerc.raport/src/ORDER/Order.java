package ORDER;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import REPORT.Visitable;
import REPORT.Visitor;

public class Order implements Visitable {
	
	private String number;
	private double orderTotalPrice=0;
	private Date orderDate;
	List<Product> productList = new ArrayList<Product>();
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	Order()
	{
		 this.orderDate = new Date();
	}
	
	void orderTotalPrice(Product product)
	{
		for(Product x : productList)
		{
			orderTotalPrice=orderTotalPrice+x.getPrice();
		}
	}
	
	
	public String getNumber()
	{
		return number;
	}
	public double getOrderTotalPrice() 
	{
		return orderTotalPrice;
	}
	public Date getOrderDate() 
	{
		return orderDate;
	}
	public void addProduct(Product a)
	{
		productList.add(a);
	}
	@Override
	public void accept(Visitor visitor) 
	{
		Visitor.visit(this);
		for(Product a : productList)
		{
			a.accept(visitor);
		}
	}
	@Override
	public String giveReport() 
	{
		return "Order: "+number+ "\nOrder total price: "+orderTotalPrice+ 
				"\nOrder date: "+format.format(orderDate);
	}
	
}
