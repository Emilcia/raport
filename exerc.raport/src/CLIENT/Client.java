package CLIENT;

import java.util.ArrayList;
import java.util.List;

import ORDER.Order;
import ORDER.Product;
import REPORT.Visitable;
import REPORT.Visitor;

public class Client implements Visitable {

	private String number;
	List<Order> orders = new ArrayList<Order>();
	
	public String getNumber() 
	{
		return number;
	}
	public void setNumber(String number) 
	{
		this.number = number;
	}
	public void addOrder(Order a)
	{
		orders.add(a);
	}
	public void accept(Visitor visitor) 
	{
		Visitor.visit(this);
		for(Order a : orders)
		{
			a.accept(visitor);
		}
	}
	@Override
	public String giveReport() 
	{
		return "Client: "+number;
	}
}
