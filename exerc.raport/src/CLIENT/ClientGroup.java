package CLIENT;

import java.util.ArrayList;
import java.util.List;

import REPORT.Visitable;
import REPORT.Visitor;

public class ClientGroup implements Visitable {
	
	public String memberName;
	List<Client> clientList = new ArrayList<Client>();
	
	public void addClient(Client a)
	{
		clientList.add(a);
	}
	public void accept(Visitor visitor) 
	{
		Visitor.visit(this);
		for(Client a : clientList)
		{
			a.accept(visitor);
		}
	}
	@Override
	public String giveReport() 
	{
		return "Group: "+memberName;
	}

}
