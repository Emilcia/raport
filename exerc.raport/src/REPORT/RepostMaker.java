package REPORT;

import CLIENT.Client;
import ORDER.Order;
import ORDER.Product;


public class RepostMaker implements Visitor {

	
	private int numberOfClients;
	private int numberOfOrders;
	private int numberOfProducts;
	public String report="";
	
	public int getNumberOfClients() 
	{
		return numberOfClients;
	}
	public int getNumberOfOrders()
	{
		return numberOfOrders;
	}
	public int getNumberOfProducts()
	{
		return numberOfProducts;
	}

	public void visit(Visitable v) 
	{
		report=report+v.giveReport();	
		
		if(v instanceof Client)
		{
			 numberOfClients+=1;
		}
		if(v instanceof Order)
		{
			 numberOfOrders+=1;
		}
		if(v instanceof Product)
		{
			 numberOfProducts+=1;
		}
	}
	public String ShowReport()
	{
		return report;
	}
}
